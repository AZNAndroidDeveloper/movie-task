package uz.azn.moviestest.features.movielist.presentation.di

import dagger.Component
import uz.azn.moviestest.activty.di.MainComponent
import uz.azn.moviestest.features.movielist.presentation.MovieListFragment

@MovieListScope
@Component(
    dependencies = [MainComponent::class],
    modules = [MovieListModule::class]
)
internal interface MovieListComponent {

    fun inject(fragment: MovieListFragment)

    @Component.Factory
    interface Factory {
        fun create(component: MainComponent): MovieListComponent
    }

    companion object {
        fun create(component: MainComponent): MovieListComponent =
            DaggerMovieListComponent
                .factory()
                .create(component)
    }
}