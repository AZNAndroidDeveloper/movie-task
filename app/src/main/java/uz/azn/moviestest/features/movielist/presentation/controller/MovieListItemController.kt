package uz.azn.moviestest.features.movielist.presentation.controller

import android.view.ViewGroup
import coil.load
import ru.surfstudio.android.easyadapter.controller.BindableItemController
import ru.surfstudio.android.easyadapter.holder.BindableViewHolder
import uz.azn.moviestest.R
import uz.azn.moviestest.databinding.ViewHolderMovieBinding
import uz.azn.moviestest.features.movielist.data.model.movie.MovieDetail
import uz.azn.moviestest.features.movielist.presentation.data.MovieDetailElement.MovieDetailItem
import uz.azn.moviestest.utils.Constant
import uz.azn.moviestest.utils.ConvertDate

internal class MovieListItemController :
    BindableItemController<MovieDetailItem, MovieListItemController.Holder>() {

    override fun createViewHolder(parent: ViewGroup): Holder = Holder(parent)

    override fun getItemId(data: MovieDetailItem): String = data.hashCode().toString()


    inner class Holder(private val parent: ViewGroup?) :
        BindableViewHolder<MovieDetailItem>(parent, R.layout.view_holder_movie) {

        private val binding =
            ViewHolderMovieBinding.bind(itemView)

        override fun bind(data: MovieDetailItem) {

            with(binding) {
                movieTitle.text = data.movieDetail.originalTitle
                movieDescription.text  = data.movieDetail.overview
                dateTextView.text = ConvertDate.covertDate(data.movieDetail.releaseDate)
                imageView.load("${Constant.URI_IMAGE}${data.movieDetail.posterPath}")
            }

        }
    }

}