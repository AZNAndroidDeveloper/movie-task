package uz.azn.moviestest.features.movielist.presentation.controller

import android.view.View
import android.view.ViewGroup
import ru.surfstudio.android.easyadapter.pagination.EasyPaginationAdapter
import ru.surfstudio.android.easyadapter.pagination.PaginationState
import uz.azn.moviestest.R
import uz.azn.moviestest.databinding.PaginationItemControllerBinding

class PaginationMovieListItemController:
    EasyPaginationAdapter.BasePaginationFooterController<PaginationMovieListItemController.Holder>() {

    override fun createViewHolder(
        parent: ViewGroup,
        listener: EasyPaginationAdapter.OnShowMoreListener
    ): Holder = Holder(parent, listener)


    inner class Holder(
        parent: ViewGroup,
        listener: EasyPaginationAdapter.OnShowMoreListener
    ) : EasyPaginationAdapter.BasePaginationFooterHolder(
        parent,
        R.layout.pagination_item_controller
    ) {
        private val binding = PaginationItemControllerBinding.bind(itemView)

        init {
            binding.progressBar.visibility = View.GONE
        }

        override fun bind(state: PaginationState) {
            with(binding) {
                when (state) {
                    PaginationState.READY -> {
                        progressBar.visibility = View.VISIBLE
                    }
                    PaginationState.COMPLETE -> {
                        progressBar.visibility = View.GONE
                    }
                    PaginationState.ERROR -> {
                        progressBar.visibility = View.GONE
                    }
                    else -> throw IllegalArgumentException("unsupported state: $state")
                }
            }
        }

    }
}