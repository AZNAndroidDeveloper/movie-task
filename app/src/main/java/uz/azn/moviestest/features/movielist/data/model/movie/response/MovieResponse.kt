package uz.azn.moviestest.features.movielist.data.model.movie.response

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MovieResponse(

    @SerialName("iso_3166_1")
    val iso31661: String,

    @SerialName("description")
    val description: String,

    @SerialName("runtime")
    val runtime: Int,

    @SerialName("average_rating")
    val averageRating: Double,

    @SerialName("sort_by")
    val sortBy: String,

    @SerialName("total_pages")
    val totalPages: Int,

    @SerialName("created_by")
    val createdBy: CreatedByResponse,

    @SerialName("iso_639_1")
    val iso6391: String,

    @SerialName("poster_path")
    val posterPath: String?,

    @SerialName("total_results")
    val totalResults: Int,

    @SerialName("backdrop_path")
    val backdropPath: String?,

    @SerialName("revenue")
    val revenue: Long,

    @SerialName("public")
    val jsonMemberPublic: Boolean,

    @SerialName("name")
    val name: String,

    @SerialName("id")
    val id: Int,

    @SerialName("page")
    val page: Int,

    @SerialName("results")
    val details: List<MovieDetailResponse>
)