package uz.azn.moviestest.features.movielist.data.mapper

import uz.azn.moviestest.features.movielist.data.model.movie.MovieDetail
import uz.azn.moviestest.features.movielist.data.model.movie.response.MovieDetailResponse
import uz.azn.moviestest.features.movielist.data.model.movie.response.MovieResponse

internal fun MovieDetailResponse.responseToMovieDetail(): MovieDetail =
    MovieDetail(
        overview = overview,
        originalLanguage = originalLanguage,
        originalTitle = originalTitle,
        video = video,
        title = title,
        genreIds = genreIds,
        posterPath = posterPath.toString(),
        backdropPath = backdropPath.toString(),
        releaseDate = releaseDate,
        popularity = popularity,
        voteAverage = voteAverage,
        id = id,
        adult = adult,
        voteCount = voteCount
    )

//internal fun MovieResponse.responseToMovieDetailList(): List<MovieDetail> =
//    this.details.map { it.responseToMovieDetail() }