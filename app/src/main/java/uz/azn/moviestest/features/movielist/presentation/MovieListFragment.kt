package uz.azn.moviestest.features.movielist.presentation

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import dagger.Lazy
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import me.vponomarenko.injectionmanager.IHasComponent
import me.vponomarenko.injectionmanager.x.XInjectionManager
import moxy.MvpAppCompatFragment
import moxy.ktx.moxyPresenter
import ru.surfstudio.android.easyadapter.EasyAdapter
import ru.surfstudio.android.easyadapter.ItemList
import ru.surfstudio.android.easyadapter.pagination.EasyPaginationAdapter
import uz.azn.moviestest.R
import uz.azn.moviestest.databinding.FragmentMovieListBinding
import uz.azn.moviestest.features.movielist.data.model.movie.MovieDetail
import uz.azn.moviestest.features.movielist.presentation.controller.MovieListItemController
import uz.azn.moviestest.features.movielist.presentation.controller.PaginationMovieListItemController
import uz.azn.moviestest.features.movielist.presentation.data.MovieDetailElement.MovieDetailItem
import uz.azn.moviestest.features.movielist.presentation.data.MovieDetailElement
import uz.azn.moviestest.features.movielist.presentation.data.MovieDetailElement.MovieDetailEmpty
import uz.azn.moviestest.features.movielist.presentation.data.MovieDetailElement.MovieDetailLoading
import uz.azn.moviestest.features.movielist.presentation.data.MovieDetailElement.MovieDetailError
import uz.azn.moviestest.features.movielist.presentation.di.MovieListComponent
import uz.azn.moviestest.utils.SimpleTextWatcher
import uz.azn.moviestest.utils.controller.StateEmptyItemController
import uz.azn.moviestest.utils.controller.StateErrorItemController
import uz.azn.moviestest.utils.controller.StateInfoItemController
import uz.azn.moviestest.utils.controller.StateLoadingItemController
import java.util.concurrent.TimeUnit
import javax.inject.Inject

internal class MovieListFragment @Inject constructor(
) : MvpAppCompatFragment(R.layout.fragment_movie_list), MovieListView,
    IHasComponent<MovieListComponent> {

    @Inject
    lateinit var lazyPresenter: Lazy<MovieListPresenter>
    private val presenter by moxyPresenter { lazyPresenter.get() }

    private val easyAdapter = EasyAdapter()
    private val easyPaginationAdapter =
        EasyPaginationAdapter(PaginationMovieListItemController()) {}
    private val movieListItemController = MovieListItemController()
    private val result = ItemList.create()
    private val movieList = arrayListOf<MovieDetail>()
    private val stateEmptyItemController = StateEmptyItemController(true)
    private val stateErrorItemController = StateErrorItemController(true, onActionClickLister = {
        presenter.getMovieList()
    })
    private val stateLoadingItemController = StateLoadingItemController(true)
    private val stateInfoItemController = StateInfoItemController(true)

    private lateinit var binding: FragmentMovieListBinding
    override fun getComponent(): MovieListComponent =
        MovieListComponent.create(XInjectionManager.findComponent())

    override fun onCreate(savedInstanceState: Bundle?) {
        XInjectionManager.bindComponent(this).inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentMovieListBinding.bind(view)
        with(binding) {
            clearImageButton.setOnClickListener {
                searchEditText.text!!.clear()
                result.clear()
            }
            recyclerView.apply {
                adapter = easyAdapter
            }
            searchEditText.addTextChangedListener(textWatcher)
        }

    }

    private val textWatcher = object : SimpleTextWatcher {
        @SuppressLint("CheckResult")
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            if (s.toString().isNotEmpty()) {
                presenter.setQuery(s.toString())
                binding.clearImageButton.visibility = View.VISIBLE
                presenter.searchMovieList()
            } else {
                result.clear()
                with(binding) {
                    clearImageButton.visibility = View.INVISIBLE
                    presenter.getMovieList()
                }
            }
        }
    }

    override fun onSuccessMovieList(movies: List<MovieDetailElement>) {
        val itemList  = ItemList.create()
        for (item in movies){
            when(item){
                is MovieDetailItem->itemList.add(item,movieListItemController)
                is MovieDetailLoading->itemList.add(stateLoadingItemController)
                is MovieDetailError->itemList.add(stateErrorItemController)
                is MovieDetailEmpty->itemList.add(stateEmptyItemController)
            }
        }
        easyAdapter.setItems(itemList)
    }

    override fun onSearchSuccessMovieList(movies: List<MovieDetailElement>) {
        val itemList = ItemList.create()
        for (item in movies){
            Log.d("TAG", "onSearchSuccessMovieList: $item")
            when(item){
                is MovieDetailItem->itemList.add(item,movieListItemController)
                is MovieDetailLoading->itemList.add(stateLoadingItemController)
                is MovieDetailError->itemList.add(stateErrorItemController)
                is MovieDetailEmpty->itemList.add(stateEmptyItemController)
            }
        }
        easyAdapter.setItems(itemList)
    }
}