package uz.azn.moviestest.features.movielist.presentation.di

import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.create
import uz.azn.moviestest.features.movielist.data.repository.MovieListRepository
import uz.azn.moviestest.features.movielist.data.repository.MovieListRepositoryImpl
import uz.azn.moviestest.features.movielist.data.rest.MovieListRestService

@Module(includes = [MovieListModule.Binders::class, MovieListModule.Providers::class])
internal object MovieListModule {

    @Module
    interface Binders {

        @Binds
        @MovieListScope
        fun bindMovieListRepository(
            impl: MovieListRepositoryImpl
        ): MovieListRepository
    }

    @Module
    object Providers {

        @Provides
        @MovieListScope
        fun providerMovieListRestService(
            retrofit: Retrofit
        ): MovieListRestService =
            retrofit.create()
    }
}