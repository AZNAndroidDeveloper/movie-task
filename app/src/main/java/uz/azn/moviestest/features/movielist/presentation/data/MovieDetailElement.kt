package uz.azn.moviestest.features.movielist.presentation.data

import uz.azn.moviestest.features.movielist.data.model.movie.MovieDetail

internal sealed class MovieDetailElement {

    data class MovieDetailItem(val movieDetail: MovieDetail) : MovieDetailElement()

    object MovieDetailLoading : MovieDetailElement()
    object MovieDetailEmpty : MovieDetailElement()
    object MovieDetailError : MovieDetailElement()
}