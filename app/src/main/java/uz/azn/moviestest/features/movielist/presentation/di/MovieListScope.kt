package uz.azn.moviestest.features.movielist.presentation.di

import javax.inject.Scope

@Scope
@Retention
annotation class MovieListScope