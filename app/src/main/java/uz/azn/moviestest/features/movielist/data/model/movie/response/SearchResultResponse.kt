package uz.azn.moviestest.features.movielist.data.model.movie.response

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
internal data class SearchResultResponse(
    @SerialName("page")
    val page: Int,
    @SerialName("results")
    val movies: List<MovieDetailResponse>,
    @SerialName("total_pages")
    val totalPages: Int?,
    @SerialName("total_results")
    val totalResults: Int?
)