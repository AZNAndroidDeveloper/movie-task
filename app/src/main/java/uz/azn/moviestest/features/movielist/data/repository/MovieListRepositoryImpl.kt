package uz.azn.moviestest.features.movielist.data.repository

import io.reactivex.Observable
import uz.azn.moviestest.features.movielist.data.mapper.responseToMovieDetail
import uz.azn.moviestest.features.movielist.data.model.movie.MovieDetail
import uz.azn.moviestest.features.movielist.data.rest.MovieListRestService
import javax.inject.Inject

internal class MovieListRepositoryImpl @Inject constructor(
    private val movieListRestService: MovieListRestService
) : MovieListRepository {

    override fun getMovieList(): Observable<List<MovieDetail>> {
        return movieListRestService.getMovieList()
            .map { it.details.map { movieDetailResponse -> movieDetailResponse.responseToMovieDetail() } }
    }

    override fun searchMovieList(query: String): Observable<List<MovieDetail>> {
        return movieListRestService.searchMovies(query = query)
            .map { it.movies.map {movieDetailResponse -> movieDetailResponse.responseToMovieDetail()} }
    }
}