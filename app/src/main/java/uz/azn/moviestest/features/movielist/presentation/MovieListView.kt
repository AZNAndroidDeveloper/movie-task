package uz.azn.moviestest.features.movielist.presentation

import moxy.MvpView
import uz.azn.moviestest.features.movielist.data.model.movie.MovieDetail
import uz.azn.moviestest.features.movielist.presentation.data.MovieDetailElement

internal interface MovieListView : MvpView {


//    fun onLoadingMovieList()

    fun onSuccessMovieList(movies: List<MovieDetailElement>)

//    fun onFailureMovieList(throwable: Throwable)


//    fun onSearchLoadingMovieList()

    fun onSearchSuccessMovieList(movies: List<MovieDetailElement>)

//    fun onSearchFailureMovieList(throwable: Throwable)
}
