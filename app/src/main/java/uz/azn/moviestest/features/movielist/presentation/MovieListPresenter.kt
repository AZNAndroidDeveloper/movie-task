package uz.azn.moviestest.features.movielist.presentation

import android.util.Log
import io.reactivex.observers.DisposableObserver
import kotlinx.coroutines.launch
import moxy.MvpPresenter
import moxy.presenterScope
import ru.surfstudio.android.datalistpagecount.domain.datalist.DataList
import uz.azn.moviestest.features.movielist.data.model.movie.MovieDetail
import uz.azn.moviestest.features.movielist.domain.MovieListInteractor
import uz.azn.moviestest.features.movielist.presentation.data.MovieDetailElement
import uz.azn.moviestest.features.movielist.presentation.data.MovieDetailElement.MovieDetailLoading
import uz.azn.moviestest.features.movielist.presentation.data.MovieDetailElement.MovieDetailItem
import uz.azn.moviestest.features.movielist.presentation.data.MovieDetailElement.MovieDetailEmpty
import uz.azn.moviestest.features.movielist.presentation.data.MovieDetailElement.MovieDetailError
import javax.inject.Inject

//typealias DataListPageCount<T> = DataList<T>
//typealias DataLimitOffset<T> = DataList<T>

internal class MovieListPresenter @Inject constructor(
    private val movieListInteractor: MovieListInteractor
) : MvpPresenter<MovieListView>() {

    override fun onFirstViewAttach() {
        getMovieList()
    }

    private val movieDetailList:MutableList<MovieDetail> = mutableListOf()
    private var movieDetailState:MovieDetailElement? =MovieDetailLoading

    private val movieDetailElements:List<MovieDetailElement>
    get(){
        val wrappers: MutableList<MovieDetailElement> = mutableListOf()
        if (movieDetailState !in arrayOf(MovieDetailError, MovieDetailEmpty, MovieDetailLoading)
        ) {
            movieDetailList.forEach { wrappers.add(MovieDetailItem(it)) }
        } else {
            movieDetailState?.let { wrappers.add(it) }
        }
        return wrappers
    }

//    fun setPage(page: Int) = movieListInteractor.setPage(page)
//
    fun setQuery(value: String) = movieListInteractor.setQuery(value)

    fun getMovieList() {
        viewState.onSuccessMovieList(listOf(MovieDetailLoading))
        presenterScope.launch {
            movieListInteractor.getMovieList()
                .subscribeWith(object : DisposableObserver<List<MovieDetail>>() {
                    override fun onNext(value: List<MovieDetail>) {
                        movieDetailList.apply { clear(); addAll(value) }
                        movieDetailState= if (movieDetailList.isNotEmpty()) null else MovieDetailEmpty
                        viewState.onSuccessMovieList(movieDetailElements)
                    }

                    override fun onError(throwable: Throwable) {
                        Log.d("Throwable", "onError: $throwable")
                        movieDetailState = MovieDetailError
                        viewState.onSuccessMovieList(movieDetailElements)
                    }

                    override fun onComplete() {}
                })
        }
    }


    fun searchMovieList() {
        viewState.onSuccessMovieList(listOf(MovieDetailLoading))
        presenterScope.launch {
            movieListInteractor.getSearchByName()
                .subscribe(object : DisposableObserver<List<MovieDetail>>() {
                    override fun onNext(t: List<MovieDetail>) {
                        Log.d("SearchMovie", "onNext: $t")
                        movieDetailList.apply { clear(); addAll(t) }
                        movieDetailState= if (movieDetailList.isNotEmpty()) null else MovieDetailEmpty
                        viewState.onSearchSuccessMovieList(movieDetailElements)
                    }

                    override fun onError(e: Throwable) {
                        movieDetailState = MovieDetailEmpty
                        viewState.onSearchSuccessMovieList(movieDetailElements)
                    }

                    override fun onComplete() {
//                        movieDetailState = MovieDetailLoading
//                        viewState.onSuccessMovieList(movieDetailElements)
                    }

                })
        }
    }

}

