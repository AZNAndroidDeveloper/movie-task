package uz.azn.moviestest.features.movielist.data.model.movie.response

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class CreatedByResponse(

    @SerialName("gravatar_hash")
    val gravatarHash: String,

    @SerialName("id")
    val id: String,

    @SerialName("username")
    val username: String
)