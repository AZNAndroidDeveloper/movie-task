package uz.azn.moviestest.features.movielist.data.repository

import io.reactivex.Observable
import uz.azn.moviestest.features.movielist.data.model.movie.MovieDetail

internal interface MovieListRepository {

    fun getMovieList(): Observable<List<MovieDetail>>

    fun searchMovieList(query:String):Observable<List<MovieDetail>>
}