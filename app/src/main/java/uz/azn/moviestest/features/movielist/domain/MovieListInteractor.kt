package uz.azn.moviestest.features.movielist.domain

import android.util.Log
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import uz.azn.moviestest.features.movielist.data.model.movie.MovieDetail
import uz.azn.moviestest.features.movielist.data.repository.MovieListRepository
import javax.inject.Inject

internal class MovieListInteractor @Inject constructor(
    private val movieListRepository: MovieListRepository
) {
    var page: Int = 0
        private set

    var query: String = ""
        private set

    fun setPage(value: Int) {
        page = value
    }

    fun setQuery(value: String) {
        query = value
        Log.d("Interator", "setPage: $query")

    }

    fun getMovieList(): Observable<List<MovieDetail>> {
        return movieListRepository.getMovieList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                Log.wtf("MLInteractor", "gotMovies onNext = value")
            }
    }
    fun getSearchByName(): Observable<List<MovieDetail>> {
        return movieListRepository.searchMovieList(query)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    }
