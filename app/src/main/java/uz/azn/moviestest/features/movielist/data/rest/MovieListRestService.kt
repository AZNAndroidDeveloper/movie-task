package uz.azn.moviestest.features.movielist.data.rest

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query
import uz.azn.moviestest.features.movielist.data.model.movie.response.MovieResponse
import uz.azn.moviestest.features.movielist.data.model.movie.response.SearchResultResponse
import uz.azn.moviestest.utils.Constant

internal interface MovieListRestService {

    @GET(Constant.API_REGISTER_UPDATE_PROFILE)
    @Headers("Authorization: Bearer ${Constant.ACCESS_TOKEN}")
    fun getMovieList(
        @Query("page") page: Int = 1
    ): Observable<MovieResponse>

    @GET("4/search/movie")
    @Headers("Authorization: Bearer ${Constant.ACCESS_TOKEN}")
    fun searchMovies(
        @Query("query") query: String,
        @Query("api_key") apiKey: String = Constant.API_KEY
    ): Observable<SearchResultResponse>
}