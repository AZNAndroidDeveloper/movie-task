package uz.azn.moviestest.activty.di

import javax.inject.Scope

@Scope
@Retention
annotation class MainScope