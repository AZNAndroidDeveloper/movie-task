package uz.azn.moviestest.activty.di

import dagger.Module
import uz.azn.moviestest.activty.di.MainModule.Binder
import uz.azn.moviestest.activty.di.MainModule.Provider

@Module(includes = [Binder::class, Provider::class])
object MainModule {

    @Module
    interface Binder {
    }

    @Module
    object Provider {
    }
}