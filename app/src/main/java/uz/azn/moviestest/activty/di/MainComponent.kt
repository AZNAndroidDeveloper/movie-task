package uz.azn.moviestest.activty.di

import dagger.Component
import retrofit2.Retrofit
import uz.azn.moviestest.activty.MainActivity
import uz.azn.moviestest.application.di.AppComponent

@MainScope
@Component(
    dependencies = [AppComponent::class],
    modules = [MainModule::class]
)
interface MainComponent {

    val retrofit: Retrofit

    fun inject(activity: MainActivity)

    @Component.Factory
    interface Factory {
        fun create(
            component: AppComponent
        ): MainComponent
    }

    companion object {
        fun create(component: AppComponent): MainComponent =
            DaggerMainComponent
                .factory()
                .create(component)
    }
}