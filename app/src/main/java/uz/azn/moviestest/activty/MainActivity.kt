package uz.azn.moviestest.activty

import android.os.Bundle
import me.vponomarenko.injectionmanager.IHasComponent
import me.vponomarenko.injectionmanager.x.XInjectionManager
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import uz.azn.moviestest.activty.di.MainComponent
import uz.azn.moviestest.databinding.ActivityMainBinding
import javax.inject.Inject

class MainActivity : MvpAppCompatActivity(), IHasComponent<MainComponent>, MainView {

    @Inject
    @InjectPresenter
    lateinit var mainPresenter: MainPresenter

    @ProvidePresenter
    fun provideGlobalPresenter(): MainPresenter = mainPresenter


    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }

    override fun getComponent(): MainComponent =
        MainComponent.create(XInjectionManager.findComponent())

    override fun onCreate(savedInstanceState: Bundle?) {
        XInjectionManager.bindComponent(this).inject(this)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
    }
}