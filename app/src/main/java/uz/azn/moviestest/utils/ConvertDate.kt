@file:Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")

package uz.azn.moviestest.utils

import java.text.SimpleDateFormat
import java.util.*

object ConvertDate {

    private val fromFormat = SimpleDateFormat("yyyy-MM-dd", Locale("ru","RU"))
    private val toFormat = SimpleDateFormat("dd MMM yyyy", Locale("ru","RU"))

    fun covertDate(date: String): String {
        return toFormat.format(fromFormat.parse(date))
    }
}