package uz.azn.moviestest.utils

import android.text.Editable
import android.text.TextWatcher

interface SimpleTextWatcher:TextWatcher {

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

    override fun afterTextChanged(s: Editable?) {}
}