package uz.azn.moviestest.application.di

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import dagger.Module
import dagger.Provides
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import uz.azn.moviestest.utils.Constant
import uz.azn.moviestest.utils.actual
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module(includes = [AppModule.Providers::class])
object AppModule {

    @Module
    object Providers {

        @Provides
        @Singleton
        fun provideOkHttpClientBuilder(
        ): OkHttpClient.Builder =
            OkHttpClient.Builder()
                .addInterceptor(HttpLoggingInterceptor())
                .retryOnConnectionFailure(true)
                .followRedirects(false)
                .followSslRedirects(false)
                .connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)

        @Provides
        @Singleton
        fun provideRetrofit(
            okHttpClientBuilder: OkHttpClient.Builder
        ): Retrofit =
            Retrofit.Builder()
                .baseUrl("${Constant.BASE_URL}")
                .client(okHttpClientBuilder.build())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(Json.actual.asConverterFactory("application/json; charset=utf-8".toMediaType()))
                .build()
    }
}