package uz.azn.moviestest.application

import android.app.Application
import android.content.Context
import com.facebook.drawee.backends.pipeline.Fresco
import me.vponomarenko.injectionmanager.IHasComponent
import me.vponomarenko.injectionmanager.x.XInjectionManager
import uz.azn.moviestest.application.di.AppComponent

class App : Application(),
    IHasComponent<AppComponent> {

    override fun getComponent(): AppComponent =
        AppComponent.create()

    override fun attachBaseContext(base: Context) {
        XInjectionManager.let { it.init(this); it.bindComponent(this).inject(this) }
        super.attachBaseContext(base)
    }

    override fun onCreate() {
        super.onCreate()
        Fresco.initialize(this)
    }
}