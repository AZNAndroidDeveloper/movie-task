package uz.azn.moviestest.application.di

import dagger.Component
import retrofit2.Retrofit
import uz.azn.moviestest.application.App
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    val retrofit: Retrofit

    fun inject(app: App)

    @Component.Factory
    interface Factory {
        fun create(): AppComponent
    }

    companion object {
        fun create(): AppComponent =
            DaggerAppComponent
                .factory()
                .create()
    }
}